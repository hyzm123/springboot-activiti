# springboot-activiti

#### 项目介绍
    流程设计平台
#### 软件架构
    基于springboot2 和 activiti-5.2
#### 使用说明
- 启动项目 
    启动时需要设置启动环境
    Cloud：云池流程
	Idc：idc流程
- swagger中创建流程
    [swagger地址](http://localhost:8181/swagger-ui.html):http://localhost:8181/swagger-ui.html
- 根据创建后返回的modelId 进入流程设计界面
    [设计地址](http://localhost:8181/modeler.html?modelId=1):http://localhost:8181/modeler.html?modelId=1

- 通过swagger导出流程xml文件


#### 参与贡献

#### 码云特技

